{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Hatlab (
    Matrix, Index(..),
    matSize,
    matrixFullOf, zeros, ones, eye, scalar, item,
    matrixFromVector, matrixFromList, matrixToList, matrix1D, matrix2D,
    arange, arange3, linspace, logspace,
    (!#), (!$), (!%), SliceReplacer((:=)),
    cat2, catN, cat, stack, unstack,
    blkdiag,
    addAxis, addAxes, addAxesToMatch,
    matfun, fmaap, matfuun,
    mlzip, mlunzip, matCouple, matDecouple,
    reduce,
    permute, transpose, matFlip, fliplr, flipud,
    diag,
    repmat, repelem,
    diagop, triu, tril,
    (×),
    mzip, munzip,
    (/.), (%), (/%), (/:), (%:), (/%:),
    (&#), (|#), mnot, (=#), (/=#), (>#), (<#), (>=#), (<=#),
    (!:), SliceUpdate,
    SliceAdd((:+=)), SliceSubtract((:-=)), SliceMultiply((:*=)), SliceDivide((:/=)), SlicePower((:**=)),
    SliceDiv((:/.=)), SliceMod((:%=)), SliceQuot((:/:=)), SliceRem((:%:=)),
    SliceAnd((:&=)), SliceOr((:|=)), SliceApply((:$=))
) where

import qualified Data.Vector
import qualified Data.List

data MatrixTraverser elemType = Value elemType
                              | Slice (Int -> MatrixTraverser elemType)

data Matrix elemType = HM (MatrixTraverser elemType) [Int]

data Index = At Int
           | From Int
           | To Int
           | FromTo Int Int
           | All
           | NewAxis
           | Where (Matrix Bool)
           | Indices (Matrix Int)
           deriving Show

-- | Return the shape of the matrix
matSize :: Matrix e -> [Int]
matSize (HM _ s) = s

traverser :: Matrix e -> MatrixTraverser e
traverser (HM tr _) = tr

scalarTraverser :: e -> MatrixTraverser e
scalarTraverser v = Value v

wi :: Int -> Int -> Int
wi size i | i >= 0 = i
          | otherwise = size + i

wis :: [Int] -> [Index] -> [Index]
wis [] [] = []
wis (sz:szs) [] = All:(wis szs [])
wis (sz:szs) (All:is) = All:(wis szs is)
wis (sz:szs) (At i:is) = (At $ wi sz i):(wis szs is)
wis (sz:szs) (From f:is) = (From $ wi sz f):(wis szs is)
wis (sz:szs) (To t:is) = (To $ wi sz t):(wis szs is)
wis (sz:szs) (FromTo f t:is) = (FromTo (wi sz f) (wi sz t)):(wis szs is)
wis s (NewAxis:is) = NewAxis:(wis s is)
wis s (Where (HM tr sz):is) = (Where (HM tr sz)):(wis (rest s sz) is)
    where rest arr [] = arr
          rest [] _ = error "Matrix of booleans has too large dimension."
          rest (a:as) (b:bs) | a == b = rest as bs
                             | otherwise = error "Matrix of booleans has incorrect size."
wis (sz:szs) (Indices m:is) = (Indices $ fmap (wi sz) m):(wis szs is)
wis [] _ = error "Too many indices given"

-- | Returns a matrix of given size such that each element is the given element.
matrixFullOf :: e -> [Int] -> Matrix e
matrixFullOf e size = HM tr size
    where addim traverser = Slice (\_ -> traverser)
          tr = (iterate addim (Value e)) !! (length size)

-- | Creates a matrix full of zeros
zeros :: Num e => [Int] -> Matrix e
zeros = matrixFullOf 0

-- | Creates a matrix full of ones
ones :: Num e => [Int] -> Matrix e
ones = matrixFullOf 1

-- | Creates an identity matrix of given size
eye :: Num e => Int -> Matrix e
eye n = HM (Slice $ \i -> Slice $ \j -> Value $ if i == j then 1 else 0) [n, n]

-- | Creates a 0-dimensional matrix containing a single element
scalar :: e -> Matrix e
scalar e = HM (Value e) []

-- | Retrieves the value of a scalar represented by 0-dimensional matrix
item :: Matrix e -> e
item (HM (Value e) []) = e
item _ = error "item: Matrix not scalar"

-- | Creates a matrix from Data.Vector.
matrixFromVector :: Data.Vector.Vector e -> [Int] -> Matrix e
matrixFromVector vec size | product size == Data.Vector.length vec = HM (select 0 size) size
                          | otherwise = error "matrixFromVector: Invalid size"
    where select vi [] = Value $ vec Data.Vector.! vi
          select vi (sz:szs) = Slice $ \i -> select (vi*sz + i) szs

-- | Creates a matrix from list
matrixFromList :: [e] -> [Int] -> Matrix e
matrixFromList l size = matrixFromVector (Data.Vector.fromList l) size

-- | Creates a list from matrix
matrixToList :: Matrix e -> [e]
matrixToList = foldr (:) []

-- | Create a 1D matrix from list, the size is detected automatically.
matrix1D :: [e] -> Matrix e
matrix1D l = matrixFromList l [length l]

-- | Create a 2D matrix from list of lists, the size is detected automatically.
matrix2D :: [[e]] -> Matrix e
matrix2D [] = HM (Slice $ \_ -> undefined) [0, 0]
matrix2D (row:rows) = m2d row rows 1 (length row)
    where m2d l [] h w = matrixFromList l [h, w]
          m2d l (r:rs) h w | length r == w = m2d (l++r) rs (h+1) w
                           | otherwise = error "matrix2D: Lists have different sizes"

-- | Create a 1D vector of elements within given range.
arange :: Real n => n -> n -> Matrix n
arange a b = matrix1D $ str a
    where str i | i >= b = []
                | otherwise = i:(str $ i+1)

-- | Create a 1D vector of elements within given range and step
arange3 :: Real n => n -> n -> n -> Matrix n
arange3 a b step = matrix1D $ str a
    where str i | i >= b = []
                | otherwise = i:(str $ i+step)

-- | Create a 1D vector of equally spaced values within given range.
linspace :: Fractional f => f -> f -> Int -> Matrix f
linspace a b l = HM (Slice (\i -> Value $ a + (realToFrac i)*s/m)) [l]
    where s = b - a
          m = realToFrac $ l - 1

-- | Create a 1D vector of logarithmically spaced values within given range.
logspace :: Floating f => f -> f -> Int -> Matrix f
logspace a b l = exp $ linspace (log a) (log b) l

infixl 1 !#
infixl 1 !$
infixr 1 !%
infixl 1 !$.
infixr 1 !%.
infixr 2 :=

-- | Returns the element at given place
(!#) :: Matrix e -> [Int] -> e
(HM (Value v) []) !# [] = v
(HM (Value _) []) !# _ = error "(!#): Too many indices given"
(HM (Slice _) (sz:szs)) !# [] = error "(!#): Too few indices given"
(HM (Slice tr) (sz:szs)) !# (i:is) | i' < 0 = error "(!#): index out of bounds"
                                   | i' >= sz = error "(!#): index out of bounds"
                                   | otherwise = (HM (tr i') szs) !# is
    where i' = wi sz i
_ !# _ = error "(!#): corrupted matrix"

expectedShape :: [Int] -> [Index] -> [Int]
expectedShape [] [] = []
expectedShape (_:szs) ((At _):ixs) = expectedShape szs ixs
expectedShape (sz:szs) (All:ixs) = sz:expectedShape szs ixs
expectedShape (sz:szs) ((From f):ixs) = (max 0 (sz - f)):expectedShape szs ixs
expectedShape (sz:szs) ((To t):ixs) = (max 0 t):expectedShape szs ixs
expectedShape (sz:szs) ((FromTo f t):ixs) = (max 0 (t - f)):expectedShape szs ixs
expectedShape s (NewAxis:ixs) = 1:(expectedShape s ixs)
expectedShape (sz:szs) ((Where m):ixs) = (sum $ fmap fromEnum m):expectedShape szs ixs
expectedShape (sz:szs) ((Indices (HM tr s)):ixs) = s ++ (expectedShape szs ixs)

snap :: (Int -> MatrixTraverser e) -> [Int] -> Int -> [Index] -> MatrixTraverser e
snap tr szs offset is = Slice $ \i -> traverser $ (HM (tr $ i+offset) szs) !$ is

-- | Returns a slice of the matrix
(!$) :: Matrix e -> [Index] -> Matrix e
(HM tr size) !$ indices = (HM tr size) !$. (wis size indices)

(!$.) :: Matrix e -> [Index] -> Matrix e
(HM (Value v) []) !$. [] = (HM (Value v) [])
(HM tr s) !$. (NewAxis:is) = (HM (Slice $ \_ -> traverser $ (HM tr s) !$ is) shape)
    where shape = expectedShape s (NewAxis:is)
(HM (Value _) []) !$. _ = error "(!$): Too many indices given"
(HM (Slice _) (sz:szs)) !$. [] = error "(!$): Too few indices given"
(HM (Slice tr) (sz:szs)) !$. ((At i):is) | i < 0 = error "(!$): index out of bounds"
                                         | i >= sz = error "(!$): index out of bounds"
                                         | otherwise = (HM (tr i) szs) !$. is
(HM (Slice tr) (sz:szs)) !$. (All:is) = HM (snap tr szs 0 is) shape
    where shape = expectedShape (sz:szs) (All:is)
(HM (Slice tr) (sz:szs)) !$. ((From f):is) = HM (snap tr szs f is) shape
    where shape = expectedShape (sz:szs) ((From f):is)
(HM (Slice tr) (sz:szs)) !$. ((To t):is) = HM (snap tr szs 0 is) shape
    where shape = expectedShape (sz:szs) ((To t):is)
(HM (Slice tr) (sz:szs)) !$. ((FromTo f t):is) = HM (snap tr szs f is) shape
    where shape = expectedShape (sz:szs) ((FromTo f t):is)
mm !$. ((Where (HM (Value True) [])):is) = error "(!$): Inexing by scalar Bool is not supported"
mm !$. ((Where (HM (Slice tr) [sz])):is) = if length slices > 0 then stack slices else void
    where trues = matrixToList $ HM (Slice tr) [sz]
          ati i = mm !$. (At i:is)
          slices = map snd $ filter fst $ zip trues $ map ati [0..(sz-1)]
          void = HM (Slice $ \i -> undefined) (0:(tail $ matSize mm))
mm !$. ((Where (HM (Slice tr) (sz:szs))):is) = cat 0 $ map (\i -> (mm !$ [At i]) !$. ((Where $ HM (tr i) szs):is)) [0..(sz-1)]
mm !$. ((Indices (HM (Value v) [])):is) = mm !$. ((At v):is)
mm !$. ((Indices (HM (Slice tr) (s:sz))):is) = HM (Slice $ \i -> traverser $ mm !$. ((Indices $ HM (tr i) sz):is)) ((s:sz) ++ (tail $ matSize mm))
_ !$. _ = error "(!$): corrupted matrix"

rsit :: (Int -> MatrixTraverser e) -> [Int] -> (Int -> MatrixTraverser e) -> [Int] -> [Index] -> Int -> Int -> MatrixTraverser e
rsit tr1 szs1 tr2 szs2 inx offset i = traverser $ m1 !%. inx := m2
    where m1 = HM (tr1 i) szs1
          m2 = HM (tr2 $ i - offset) szs2

data SliceReplacer e = [Index] := Matrix e

-- | Returns a new matrix with an updated slice.
-- e.g. m !% [At 2, All] := ones [3] 
(!%) :: Matrix e -> SliceReplacer e -> Matrix e
(HM tr size) !% indices := mat = (HM tr size) !%. (wis size indices) := mat

cumsum :: [Int] -> [Int]
cumsum [] = [0]
cumsum (a:as) = 0:(map (+a) $ cumsum as)

(!%.) :: Matrix e -> SliceReplacer e -> Matrix e
(HM (Value v1) []) !%. [] := (HM (Value v2) []) = HM (Value v2) []
mat !%. (NewAxis:is) := (HM (Slice tr) (1:szs)) = mat !%. is := (HM (tr 0) szs)
_ !%. (NewAxis:_) := _ = error "(!%): Sizes do not match"
(HM (Value _) []) !%. _ := _ = error "(!%): Too many indices given"
(HM (Slice _) []) !%. [] := _ = error "(!%): Too few indices given"
_ !%. [] := (HM (Slice _) []) = error "(!%): Right hand matrix has too many dimensions."
(HM (Slice tr) (sz:szs)) !%. ((At i):is) := m | i < 0 = error "(!%): index out of bounds"
                                              | i >= sz = error "(!%): index out of bounds"
                                              | otherwise = HM (Slice tr') (sz:szs)
    where tr' j | i == j = traverser $ (HM (tr i) (szs)) !%. is := m
                | otherwise = tr j
_ !%. _ := (HM (Value _) []) = error "(!%): Right hand matrix has too few dimensions."
(HM (Slice tr) (sz:szs)) !%. (All:is) := (HM (Slice tr2) (sz2:szs2)) | sz == sz2 = HM (Slice tr') (sz:szs)
                                                                     | otherwise = error "(!%): Sizes do not match"
    where tr' = rsit tr szs tr2 szs2 is 0
(HM (Slice tr) (sz:szs)) !%. ((From f):is) := (HM (Slice tr2) (sz2:szs2)) | ssz == sz2 = HM (Slice tr') (sz:szs)
                                                                          | otherwise = error "(!%): Sizes do not match"
    where ssz = max 0 (sz - f)
          tr' j | j < f = tr j
                | otherwise = rsit tr szs tr2 szs2 is f j
(HM (Slice tr) (sz:szs)) !%. ((To t):is) := (HM (Slice tr2) (sz2:szs2)) | ssz == sz2 = HM (Slice tr') (sz:szs)
                                                                        | otherwise = error "(!%): Sizes do not match"
    where ssz = max 0 t
          tr' j | j >= t = tr j
                | otherwise = rsit tr szs tr2 szs2 is 0 j
(HM (Slice tr) (sz:szs)) !%. ((FromTo f t):is) := (HM (Slice tr2) (sz2:szs2)) | ssz == sz2 = HM (Slice tr') (sz:szs)
                                                                              | otherwise = error "(!%): Sizes do not match"
    where ssz = max 0 (t - f)
          tr' j | j < f || j >= t = tr j
                | otherwise = rsit tr szs tr2 szs2 is f j
mat !%. ((Where (HM (Value True) [])):is) := _ = error "(!$): Inexing by scalar Bool is not supported"
m1 !%. ((Where mi):is) := m2 = stack slices
    where (HM (Slice tri) (szi:szis)) = mi
          tcount = matrixToList $ reduce sum (fmap fromEnum mi) [1..(length szis)]
          fcount = matrixToList $ reduce sum (fmap (fromEnum . not) mi) [1..(length szis)]
          ctcs = cumsum tcount
          os = map (\i -> m1 !$ [At i]) [0..(szi-1)]
          xs = map (\i -> mi !$ [At i]) [0..(szi-1)]
          ns = map (\(i1, i2) -> (m2 !$ [FromTo i1 i2])) $ zip ctcs $ tail ctcs
          slices = aux $ map (\(f, (t, (o, (x, n)))) -> (f, t, o, x, n)) $ zip fcount $ zip tcount $ zip os $ zip xs ns
          aux [] = []
          aux ((0, _, o, _, n):sls) = (matCouple $ matrixFromList (unstack n) (matSize o)):(aux sls)
          aux ((_, 0, o, _, n):sls) = o:(aux sls)
          aux ((_, _, o, x, n):sls) = (o !%. ((Where x):is) := n):(aux sls)
m1 !%. ((Indices (HM (Value i) [])):is) := m2 = m1 !%. ((At i):is) := m2
m1 !%. ((Indices mi):is) := m2 = stack slices
    where s1 = head $ matSize m1
          mis = matSize mi
          di = length mis
          aa2 0 m = m
          aa2 n (HM (Value v) []) = (HM (Slice $ \i -> atr) (1:asz))
              where (HM atr asz) = aa2 (n-1) (HM (Value v) [])
          aa2 n (HM (Slice tr) (sz:szs)) = (HM (Slice atr) ((sz:szs)++(take n $ repeat 1)))
              where atr i = traverser $ aa2 n (HM (tr i) szs)
          coords = fmap (fmap $ \a -> [At a]) $ map (\i -> aa2 (di-i-1) $ matrix1D [0..((mis !! i)-1)]) [0..(di-1)]
          mg = foldr (matfun (++)) (scalar []) coords
          ics = Data.List.sortBy (\(a, _) (b, _) -> if a < b then LT else GT) $ (matrixToList $ mzip mi mg)
          aux [] i | i >= s1 = []
                   | otherwise = (m1 !$ [At i]):(aux [] (i+1))
          aux ((c,w):cs) i | c < i = aux cs i
                           | c == i = ((m1 !$ [At i]) !% is := (m2 !$ w)):(aux cs (i+1))
                           | otherwise = (m1 !$ [At i]):(aux ((c,w):cs) (i+1))
          slices = aux ics 0
_ !%. _ = error "(!%): corrupted matrix"

cat_dims2 :: [Int] -> Int -> [Int] -> [Int] -> (Bool, [Int])
cat_dims2 r n [] [] | n >= 0 = error "cat2: Dimension out of range"
                    | otherwise = (True, reverse r)
cat_dims2 _ _ [] _ = error "cat2: Matrix 1 has too small dimension"
cat_dims2 _ _ _ [] = error "cat2: Matrix 2 has too small dimension"
cat_dims2 r 0 (a:as) (b:bs) = cat_dims2 ((a+b):r) (-1) as bs
cat_dims2 r n (a:as) (b:bs) | a == b = cat_dims2 (a:r) (n-1) as bs
                            | otherwise = (False, [])

same_dims2 :: [Int] -> [Int] -> Bool
same_dims2 [] [] = True
same_dims2 _ [] = False
same_dims2 [] _ = False
same_dims2 (a:as) (b:bs) = (a == b) && (same_dims2 as bs)

same_dims :: [[Int]] -> Bool
same_dims [] = True
same_dims [s] = True
same_dims [a,b] = same_dims2 a b
same_dims (a:b:cs) = (same_dims2 a b) && (same_dims (b:cs))

clever_merge :: Eq c => (a -> a -> a) -> ([a] -> a) -> (a -> c) -> a -> [a] -> a
clever_merge _ _ _ err [] = err
clever_merge _ _ _ _ [a] = a
clever_merge merge2 mergeN ext err l = clever_merge merge2 mergeN ext err $ cm [] l
    where cm [] [] = []
          cm [] [a] = [a]
          cm [] (a:as) = cm [a] as
          cm [b] [] = [b]
          cm [b2,b1] [] = [merge2 b1 b2]
          cm b [] = [mergeN $ reverse b]
          cm [b] (a:as) = if (ext a) == (ext b) then cm [a,b] as else (merge2 b a):(cm [] as)
          cm (b:bs) (a:as) = if (ext a) == (ext b) then cm (a:b:bs) as else (mergeN $ reverse (b:bs)):(cm [] (a:as))

-- | Concatenate two arrays in given dimension
cat2 :: Int -> Matrix e -> Matrix e -> Matrix e
cat2 n (HM t1 s1) (HM t2 s2) | ok = (HM (concat n t1 s1 t2) s)
                             | otherwise = error "cat2: Sizes of matrices don't match"
    where (ok, s) = cat_dims2 [] n s1 s2
          concat 0 (Slice tr1) (sz1:szs1) (Slice tr2) = Slice tr
              where tr i | i < sz1 = tr1 i
                         | otherwise = tr2 $ i - sz1
          concat n (Slice tr1) (sz1:szs1) (Slice tr2) = Slice tr
              where tr i = concat (n-1) (tr1 i) szs1 (tr2 $ i)

all_equal :: Eq a => [a] -> Bool
all_equal [] = True
all_equal [a] = True
all_equal (a1:a2:as) = a1 == a2 && (all_equal (a2:as))

-- | Effective concatenation of N matrices with equal size
catN :: Int -> [Matrix e] -> Matrix e
catN n [] = HM (Slice $ \i -> undefined) $ (0 : (take (n) $ repeat 1))
catN n [m] = m
catN n mats | all_equal $ map matSize mats = HM (concat n $ map traverser mats) (new_size n $ matSize $ mats !! 0)
            | otherwise = error "catN: Sizes of matrics don't match"
    where l = length mats
          new_size 0 (a:as) = (a*l):as
          new_size d (a:as) = a:(new_size (d-1) as)
          s = (matSize $ mats !! 0) !! n
          concat 0 trs = Slice $ \i -> let (k, o) = i `divMod` s in (vec Data.Vector.! k) o
              where vec = Data.Vector.fromList $ map (\(Slice trf) -> trf) trs
          concat d trs = Slice $ \i -> concat (d-1) $ map (\(Slice trf) -> trf i) trs

-- | Concatenate an array of arrays into one array in given dimension
cat :: Int -> [Matrix e] -> Matrix e
cat n = clever_merge (cat2 n) (catN n) ext err
    where err = HM (Slice $ \i -> undefined) $ (0 : (take (n) $ repeat 1))
          ext (HM _ s) = s !! n

-- | Stack an array of matrices in a newly created dimension
stack :: [Matrix e] -> Matrix e
stack [] = HM (Slice $ \i -> undefined) [0]
stack ms | same_dims $ map matSize ms = HM (Slice $ \i -> vec Data.Vector.! i) ((length ms):(matSize $ head ms))
         | otherwise = error "stack: Matrices have different sizes"
    where vec = Data.Vector.fromList $ map traverser ms

-- | The opposite of stack
unstack :: Matrix e -> [Matrix e]
unstack (HM (Value _) _) = error "unstack: cannot unstack a scalar"
unstack (HM (Slice tr) (s:sz)) = map (\i -> HM (tr i) sz) [0..(s-1)]

blkdiag2 :: Num e => Matrix e -> Matrix e -> Matrix e
blkdiag2 m1 m2 = cat 0 $ map (cat 1) [[m1, zeros [a,y]], [zeros [x,b], m2]]
    where [a,b] = matSize m1
          [x,y] = matSize m2

blkdiagN :: Num e => [Matrix e] -> Matrix e
blkdiagN [] = HM (Slice $ \i -> undefined) [0,0]
blkdiagN [m] = m
blkdiagN ms | all_equal $ map matSize ms = HM (Slice tr1) [n*r, n*c]
            | otherwise = error "blkdiagN: Sizes of matrices are not equal"
    where [r,c] = matSize $ ms !! 0
          n = length ms
          vec = Data.Vector.fromList $ map ((\(Slice x) -> x) . traverser) ms
          tr1 i = Slice tr2
              where (vi,mi) = i `divMod` r
                    Slice vtr = (vec Data.Vector.! vi) mi
                    tr2 j | j < vi*c = Value 0
                          | j >= (vi+1)*c = Value 0
                          | otherwise = vtr $ j - vi*c

-- | Compose a block-wise diagonal matrix from given blocks
blkdiag :: Num e => [Matrix e] -> Matrix e
blkdiag = clever_merge blkdiag2 blkdiagN matSize $ HM (Slice $ \i -> undefined) [0,0]

-- | Add one axis to the array
addAxis :: Matrix e -> Matrix e
addAxis (HM tr s) = HM (Slice $ \_ -> tr) (1:s)

-- | Add given number of axes to the array
addAxes :: Int -> Matrix e -> Matrix e
addAxes n m = (iterate addAxis m) !! n

-- | Add as many axes to the matrices to make their dimension the same
addAxesToMatch :: Matrix a -> Matrix b -> (Matrix a, Matrix b)
addAxesToMatch (HM tr1 s1) (HM tr2 s2) | d1 == d2 = ((HM tr1 s1), (HM tr2 s2))
                                       | d1 > d2 = ((HM tr1 s1), (addAxes (d1-d2) $ HM tr2 s2))
                                       | otherwise = ((addAxes (d2-d1) $ HM tr1 s1), (HM tr2 s2))
    where d1 = length s1
          d2 = length s2

-- | Transform a binary operator to a matrix operator
matfun :: (a -> b -> c) -> Matrix a -> Matrix b -> Matrix c
matfun fun m1 m2 = matfun' m1' m2'
    where (m1', m2') = addAxesToMatch m1 m2
          newSize [] [] = []
          newSize (1:as) (b:bs) = b:(newSize as bs)
          newSize (a:as) (b:bs) = a:(newSize as bs)
          matfun' (HM (Value v1) []) (HM (Value v2) []) = (HM (Value $ fun v1 v2) [])
          matfun' (HM (Value v1) _) _ = error "matfun: corrupted matrix 1"
          matfun' _ (HM (Value v2) _) = error "matfun: corrupted matrix 2"
          matfun' (HM (Slice tr1) (1:szs1)) (HM (Slice tr2) (1:szs2)) = (HM (Slice $ \_ -> tr) (1:szs))
              where HM tr szs = matfun' (HM (tr1 0) szs1) (HM (tr2 0) szs2)
          matfun' (HM (Slice tr1) (a:szs1)) (HM (Slice tr2) (1:szs2)) = (HM (Slice $ tr) (a:szs))
              where tr i = traverser $ matfun' (HM (tr1 i) szs1) (HM (tr2 0) szs2)
                    szs = newSize szs1 szs2
          matfun' (HM (Slice tr1) (1:szs1)) (HM (Slice tr2) (b:szs2)) = (HM (Slice $ tr) (b:szs))
              where tr i = traverser $ matfun' (HM (tr1 0) szs1) (HM (tr2 i) szs2)
                    szs = newSize szs1 szs2
          matfun' (HM (Slice tr1) (a:szs1)) (HM (Slice tr2) (b:szs2)) | a == b = (HM (Slice $ tr) (a:szs))
                                                                      | otherwise = error "matfun: uncompatible matrix sizes"
              where tr i = traverser $ matfun' (HM (tr1 i) szs1) (HM (tr2 i) szs2)
                    szs = newSize szs1 szs2

-- | Apply a matrix function to all slices of given dimension
fmaap :: Int -> (Matrix a -> Matrix b) -> Matrix a -> Matrix b
fmaap d fun m = matCouple $ fmap fun $ matDecouple d m

-- | Apply a binary matrix operator to all slices of given dimensions, use broadcasting if applicable
matfuun :: Int -> Int -> (Matrix a -> Matrix b -> Matrix c) -> Matrix a -> Matrix b -> Matrix c
matfuun d1 d2 fun m1 m2 = matCouple $ matfun fun (matDecouple d1 m1) (matDecouple d2 m2)

-- | Creates a matrix of lists from a list of matrices
mlzip :: [Matrix a] -> Matrix [a]
mlzip [] = scalar []
mlzip (m:mz) = matfun (:) m (mlzip mz)

-- | Creates a list of matrices from a matrix of list
mlunzip :: Matrix [a] -> [Matrix a]
mlunzip m | all empty m = []
          | any empty m = error "mlunzip: Elements have different lengths"
          | otherwise = (fmap head m):(mlunzip $ fmap tail m)
    where empty [] = True
          empty _ = False

matCouple' :: Matrix(Matrix a) -> [Int] -> Matrix a
matCouple' (HM (Value m) _) _ = m
matCouple' (HM (Slice tr) (s:sz)) sz2 = HM (Slice (\i -> traverser $ matCouple' (HM (tr i) sz) sz2)) ((s:sz)++sz2)

-- | Creates a big matrix from matrix of matrices
matCouple :: Matrix(Matrix a) -> Matrix a
matCouple (HM (Value m) _) = m
matCouple m | (product sm) == 0 = HM (Slice (\i -> undefined)) sm
            | all_equal $ sz = matCouple' m $ (sz !! 0)
            | otherwise = error "matCouple: sizes of matrices are not equal"
    where sm = matSize m
          sz = matrixToList $ fmap matSize $ m

matDecouple' :: Int -> Matrix a -> Matrix(Matrix a)
matDecouple' 0 m = scalar m
matDecouple' n (HM (Slice tr) s) = HM (Slice $ \i -> traverser $ matDecouple' (n-1) $ HM (tr i) (tail s)) $ take n s

-- | Creates a matrix of N-dimensional matrices from a big matrix.
matDecouple :: Int -> Matrix a -> Matrix(Matrix a)
matDecouple n m | n > dim = error "matDecouple: insufficient matrix dimension"
                | otherwise = matDecouple' (dim-n) m
    where dim = length $ matSize m

red :: Matrix a -> [Bool] -> Matrix [a]
red mt [] = mlzip [mt]
red (HM (Slice tr) (sz:szs)) (False:ds) = HM (Slice $ \i -> traverser $ red (HM (tr i) szs) ds) (sz:szs)
red (HM (Slice tr) (sz:szs)) (True:ds) = fmap (foldr (++) []) $ mlzip $ map (\i -> red (HM (tr i) szs) ds) [0..(sz-1)]

-- | Reduces the matrix by given reduction in given axes
reduce :: ([a] -> b) -> Matrix a -> [Int] -> Matrix b
reduce f m dims = fmap f $ red m db
    where dim = length $ matSize m
          dims' = map (\i -> if i < 0 then dim + i else i) dims
          db = map (\i -> elem i dims') [0..(dim-1)]

-- | Permutes the axes in the matrix
permute :: [Int] -> Matrix a -> Matrix a
permute [] (HM (Value v) []) = HM (Value v) []
permute (p:ps) m = HM (Slice $ \i -> traverser $ permute ps2 $ m !$ ((take p $ repeat All)++[At i])) s2
    where s2 = map ((matSize m)!!) (p:ps)
          ps2 = map psm ps
          psm i | i > p = i - 1
                | i < p = i
                | otherwise = error "permute: invalid permutation"
permute _ _ = error "permute: invalid permutation"

-- | Transposes the matrix or vector
transpose :: Matrix a -> Matrix a
transpose (HM (Value v) []) = HM (Value v) []
transpose (HM (Slice tr) [s]) = HM (Slice $ \i -> Slice $ \j -> tr i) [s,1]
transpose m = let d = length $ matSize m in permute ([0..(d-3)] ++ [d-1,d-2]) m

-- | Flip the matrix in given axis
matFlip :: Int -> Matrix a -> Matrix a
matFlip a m | a' >= dim = error "flip: invalid axis"
            | otherwise = flip' a' m
    where dim = length $ matSize m
          a' = if a >= 0 then a else a+dim
          flip' 0 (HM (Slice tr) (s:sz)) = HM (Slice (\i -> tr $ s-i-1)) (s:sz)
          flip' ax (HM (Slice tr) (s:sz)) = HM (Slice (\i -> traverser $ flip' (ax-1) $ (HM (tr i) sz))) (s:sz)

-- | Flip the matrix horizontally
fliplr :: Matrix a -> Matrix a
fliplr = matFlip (-1)

-- | Flip the matrix vertically
flipud :: Matrix a -> Matrix a
flipud = matFlip (-2)


-- | Create a diagonal matrix
diag :: Num a => Matrix a -> Matrix a
diag (HM (Value v) []) = matrix2D [[v]]
diag (HM (Slice tr) [s]) = HM (Slice $ \i -> Slice $ \j -> if i == j then tr i else Value 0) [s,s]
diag _ = error "diag: building diagonal matrices from matriced with D>1 is not supported"

stretched_size :: [Int] -> [Int] -> [Int]
stretched_size [] sz = sz
stretched_size (a:as) (b:bs) = (a*b):(stretched_size as bs)

-- | Repeat the matrix given number of times in given dimensions
repmat :: [Int] -> Matrix a -> Matrix a
repmat ns m | d1 > d2 = repmat' ns $ addAxes (d1-d2) m
            | otherwise = repmat' ns m
    where d1 = length ns
          d2 = length $ matSize m
          repmat' [] m' = m'
          repmat' (a:as) (HM (Slice tr) (sz:szs)) = HM (Slice tr2) $ (a*sz):stss
              where stss = stretched_size as szs
                    tr2 i = traverser $ repmat' as $ HM (tr $ i `mod` sz) szs

-- | Repeat the elements of matrix given number of times in given dimensions
repelem :: [Int] -> Matrix a -> Matrix a
repelem ns m | d1 > d2 = repelem' ns $ addAxes (d1-d2) m
             | otherwise = repelem' ns m
    where d1 = length ns
          d2 = length $ matSize m
          repelem' [] m' = m'
          repelem' (a:as) (HM (Slice tr) (sz:szs)) = HM (Slice tr2) $ (a*sz):stss
              where stss = stretched_size as szs
                    tr2 i = traverser $ repelem' as $ HM (tr $ i `div` a) szs

-- | Mask elements based on the distance from the main diagonal
diagop :: (Matrix Int -> Matrix Int -> Matrix Bool) -> a -> Int -> Matrix a -> Matrix a
diagop whf el n (HM (Slice t) [r,c]) = m !:[Where $ whf mask (scalar n)]:= scalar el
    where m = HM (Slice t) [r,c]
          mask = (arange 0 c) - (transpose $ arange 0 r)
diagop _ _ _ _ = error "diagop: Only 2D matrices are supported"

-- | Retrieve the upper triangular part of the matrix
triu :: Num a => Int -> Matrix a -> Matrix a
triu n = fmaap 2 $ diagop (<#) 0 n

-- | Retrieve the lower triangular part of the matrix
tril :: Num a => Int -> Matrix a -> Matrix a
tril n = fmaap 2 $ diagop (>#) 0 n

(×.) :: Num a => Matrix a -> Matrix a -> Matrix a
(HM (Value a) []) ×. (HM (Value b) []) = scalar $ a*b
(HM (Slice t) [a]) ×. (HM (Value b) []) = (HM (Slice t) [a]) * (HM (Value b) [])
(HM (Slice t) [a,b]) ×. (HM (Value u) []) = (HM (Slice t) [a,b]) * (HM (Value u) [])
(HM (Value a) []) ×. (HM (Slice u) [x]) = (HM (Value a) []) * (HM (Slice u) [x])
(HM (Slice t) [a]) ×. (HM (Slice u) [x]) = error "(×): Matrix multiplication of two row vectors is not defined"
(HM (Slice t) [a,b]) ×. (HM (Slice u) [x]) | x == b = matrix1D $ map elem [0..(a-1)]
                                           | otherwise = error "(×): Matrix dimensions do not match"
    where m = (HM (Slice t) [a,b])
          v = (HM (Slice u) [x])
          elem i = sum $ map (\j -> (m !# [i,j]) * (v !# [j])) [0..(b-1)]
(HM (Value a) []) ×. (HM (Slice u) [x,y]) = (HM (Value a) []) * (HM (Slice u) [x,y])
(HM (Slice t) [a]) ×. (HM (Slice u) [x,y]) | a == x = matrix1D $ map elem [0..(y-1)]
                                           | otherwise = error "(×): Matrix dimensions do not match"
    where m = (HM (Slice u) [x,y])
          v = (HM (Slice t) [a])
          elem i = sum $ map (\j -> (v !# [j]) * (m !# [j,i])) [0..(a-1)]
(HM (Slice t) [a,b]) ×. (HM (Slice u) [x,y]) | b == x = matrix2D $ map (\i -> map (elem i) [0..(y-1)]) [0..(a-1)]
                                             | otherwise = error "(×): Matrix dimensions do not match"
    where m = (HM (Slice t) [a,b])
          n = (HM (Slice u) [x,y])
          elem i j = sum $ map (\k -> (m !# [i,k]) * (n !# [k,j])) [0..(x-1)]
_ ×. _ = error "(×.): Matrices D>2 are not supported"

-- | Permorm matrix multiplication
(×) :: Num a => Matrix a -> Matrix a -> Matrix a
a × b | d1 <= 2 && d2 <= 2 = a ×. b
      | d1 <= 2 = fmaap 2 (a×) b
      | d2 <= 2 = fmaap 2 (×b) a
      | otherwise = matfuun 2 2 (×) a b
    where d1 = length $ matSize a
          d2 = length $ matSize b

infixl 7 ×.
infixl 7 ×

-- | Parse the matrix into a string
instance Show e => Show (Matrix e) where
    show (HM (Value v) _) = show v
    show (HM (Slice trf) (sz:szs)) = "[" ++ (fst $ foldr (\a (b, t) -> (a++t++b, ",")) ("", "") $ map (\i -> show (HM (trf i) szs)) [0..(sz-1)]) ++ "]"
    show (HM (Slice trf) []) = error "show: corrupted matrix"

instance Functor MatrixTraverser where
    fmap fun (Value v) = Value $ fun v
    fmap fun (Slice tr) = Slice $ \i -> fmap fun $ tr i

-- | mapping for matrices
instance Functor Matrix where
    fmap fun (HM tr s) = HM (fmap fun tr) s

-- | foldl and foldr reductions for matrices
instance Foldable Matrix where
    foldr f v0 (HM (Value v) _) = f v v0
    foldr f v0 (HM (Slice tr) (s:sz)) = foldr_ (s-1) v0
        where foldr_ i v | i >= 0 = foldr_ (i-1) $ foldr f v $ HM (tr i) sz
                         | otherwise = v
    foldMap mon mat = foldr mappend mempty $ fmap mon mat
    foldl f v0 (HM (Value v) _) = f v0 v
    foldl f v0 (HM (Slice tr) (s:sz)) = foldl_ 0 v0
        where foldl_ i v | i < s = foldl_ (i+1) $ foldl f v $ HM (tr i) sz
                         | otherwise = v

-- | Numeric operations are taken element-wise. Function fromInteger creates a scalar (0D matrix).
instance Num a => Num (Matrix a) where
    (+) = matfun (+)
    (-) = matfun (-)
    (*) = matfun (*)
    negate = fmap negate
    abs = fmap abs
    signum = fmap signum
    fromInteger = scalar . fromInteger

-- | Numeric operations are taken element-wise. Function fromRational creates a scalar (0D matrix).
instance Fractional a => Fractional (Matrix a) where
    (/) = matfun (/)
    recip = fmap recip
    fromRational = scalar . fromRational

-- | All these numeric operations are taken element-wise.
instance Floating a => Floating (Matrix a) where
    pi = scalar pi
    exp = fmap exp
    log = fmap log
    sqrt = fmap sqrt
    (**) = matfun (**)
    logBase = matfun logBase
    sin = fmap sin
    cos = fmap cos
    tan = fmap tan
    asin = fmap asin
    acos = fmap acos
    atan = fmap atan
    sinh = fmap sinh
    cosh = fmap cosh
    tanh = fmap tanh
    asinh = fmap asinh
    acosh = fmap acosh
    atanh = fmap atanh

-- | Matrices are equal if and only if they have the same size and same content.
instance Eq a => Eq (Matrix a) where
    a == b = (matSize a) == (matSize b) && (all id $ a =# b)

-- | Create a matrix of tuples from two matrices
mzip :: Matrix a -> Matrix b -> Matrix (a, b)
mzip = matfun $ \a b -> (a, b)

-- | Create two matrices from a matrix of tuples
munzip :: Matrix (a, b) -> (Matrix a, Matrix b)
munzip m = (fmap fst m, fmap snd m)

-- | Integer division rounded down
(/.) :: Integral a => Matrix a -> Matrix a -> Matrix a
(/.) = matfun div
infixl 7 /.

-- | Modulo with rounding down
(%) :: Integral a => Matrix a -> Matrix a -> Matrix a
(%) = matfun mod
infixl 7 %

-- | Integer division and modulo with rounding down
(/%) :: Integral a => Matrix a -> Matrix a -> (Matrix a, Matrix a)
a /% b = munzip $ matfun divMod a b
infix 7 /%

-- | Integer division rounded towards zero
(/:) :: Integral a => Matrix a -> Matrix a -> Matrix a
(/:) = matfun quot
infixl 7 /:

-- | Modulo with rounding towards zero
(%:) :: Integral a => Matrix a -> Matrix a -> Matrix a
(%:) = matfun rem
infixl 7 %:

-- | Integer division and modulo with rounding towards zero
(/%:) :: Integral a => Matrix a -> Matrix a -> (Matrix a, Matrix a)
a /%: b = munzip $ matfun quotRem a b
infix 7 /%:

-- | Element-wise AND operator
(&#) :: Matrix Bool -> Matrix Bool -> Matrix Bool
(&#) = matfun (&&)
infixr 3 &#

-- | Element-wise OR operator
(|#) :: Matrix Bool -> Matrix Bool -> Matrix Bool
(|#) = matfun (||)
infixr 2 |#

-- | Element-wise NOT operator
mnot :: Matrix Bool -> Matrix Bool
mnot = fmap not

-- | Element-wise equality comparision
(=#) :: Eq a => Matrix a -> Matrix a -> Matrix Bool
(=#) = matfun (==)
infix 4 =#

-- | Element-wise inequality comparision
(/=#) :: Eq a => Matrix a -> Matrix a -> Matrix Bool
(/=#) = matfun (/=)
infix 4 /=#

-- | Element-wise greater-than comparision
(>#) :: Ord a => Matrix a -> Matrix a -> Matrix Bool
(>#) = matfun (>)
infix 4 >#

-- | Element-wise less-than comparision
(<#) :: Ord a => Matrix a -> Matrix a -> Matrix Bool
(<#) = matfun (<)
infix 4 <#

-- | Element-wise greater-or-equal comparision
(>=#) :: Ord a => Matrix a -> Matrix a -> Matrix Bool
(>=#) = matfun (>=)
infix 4 >=#

-- | Element-wise less-or-equal comparision
(<=#) :: Ord a => Matrix a -> Matrix a -> Matrix Bool
(<=#) = matfun (<=)
infix 4 <=#

class SliceUpdate su e where
    updateWhere :: su e -> [Index]
    updateHow :: Matrix e -> su e -> Matrix e

-- | Syntactic sugar for easier slice replacement
(!:) :: SliceUpdate su e => Matrix e -> su e -> Matrix e
m !: su = m !% (updateWhere su) := (updateHow (m !$ updateWhere su) su)
infixr 1 !:

-- | Replace the elements but in a smarter way
instance SliceUpdate SliceReplacer e where
    updateWhere (i := _) = i
    updateHow m1 (_ := m2) = matfun (\a b -> b) m1 m2

-- | Addition to selected elements
data SliceAdd e = [Index] :+= Matrix e
instance Num e => SliceUpdate SliceAdd e where
    updateWhere (i :+= _) = i
    updateHow a (_ :+= b) = a + b
infixr 2 :+=

-- | Subtraction to selected elements
data SliceSubtract e = [Index] :-= Matrix e
instance Num e => SliceUpdate SliceSubtract e where
    updateWhere (i :-= _) = i
    updateHow a (_ :-= b) = a - b
infixr 2 :-=

-- | Multiply selected elements
data SliceMultiply e = [Index] :*= Matrix e
instance Num e => SliceUpdate SliceMultiply e where
    updateWhere (i :*= _) = i
    updateHow a (_ :*= b) = a * b
infixr 2 :*=

-- | Divide selected elements
data SliceDivide e = [Index] :/= Matrix e
instance Fractional e => SliceUpdate SliceDivide e where
    updateWhere (i :/= _) = i
    updateHow a (_ :/= b) = a / b
infixr 2 :/=

-- | Power selected elements
data SlicePower e = [Index] :**= Matrix e
instance Floating e => SliceUpdate SlicePower e where
    updateWhere (i :**= _) = i
    updateHow a (_ :**= b) = a ** b
infixr 2 :**=

-- | Integral division of selected elements rounded down
data SliceDiv e = [Index] :/.= Matrix e
instance Integral e => SliceUpdate SliceDiv e where
    updateWhere (i :/.= _) = i
    updateHow a (_ :/.= b) = a % b
infixr 2 :/.=

-- | Modulo of selected elements with rounding down
data SliceMod e = [Index] :%= Matrix e
instance Integral e => SliceUpdate SliceMod e where
    updateWhere (i :%= _) = i
    updateHow a (_ :%= b) = a % b
infixr 2 :%=

-- | Integral division of selected elements rounded towards zero
data SliceQuot e = [Index] :/:= Matrix e
instance Integral e => SliceUpdate SliceQuot e where
    updateWhere (i :/:= _) = i
    updateHow a (_ :/:= b) = a /: b
infixr 2 :/:=

-- | Modulo of selected elements with rounding towards zero
data SliceRem e = [Index] :%:= Matrix e
instance Integral e => SliceUpdate SliceRem e where
    updateWhere (i :%:= _) = i
    updateHow a (_ :%:= b) = a %: b
infixr 2 :%:=

-- | Conjunction of selected elements
data SliceAnd e = [Index] :&= Matrix e
instance SliceUpdate SliceAnd Bool where
    updateWhere (i :&= _) = i
    updateHow a (_ :&= b) = a &# b
infixr 2 :&=

-- | Disjunction of selected elements
data SliceOr e = [Index] :|= Matrix e
instance SliceUpdate SliceOr Bool where
    updateWhere (i :|= _) = i
    updateHow a (_ :|= b) = a |# b
infixr 2 :|=

-- | Apply a function to selected elements
data SliceApply e = [Index] :$= (Matrix e -> Matrix e)
instance SliceUpdate SliceApply e where
    updateWhere (i :$= _) = i
    updateHow a (_ :$= f) = f a
infixr 2 :$=

